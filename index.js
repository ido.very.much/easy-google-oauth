import input from 'input';
import createApp from './src/createApp.js';
import generateUserToken from './src/generateUserToken.js';
import deleteApp from './src/deleteApp.js';
import listApps from './src/listApps.js';
import deleteAllApps from './src/deleteAllApps.js';

const next = await input.select('What do you want to do?', [
    { name: 'Create new project', value: 'app' }, 
    { name: 'Connect new user', value: 'user' },
    { name: 'Delete project', value: 'delete' },
    { name: 'Delete all projects', value: 'delete-all' },
    { name: 'List projects', value: 'list' },
]);

switch (next) {
    case 'app':
        await createApp();
        break;
    case 'user':
        await generateUserToken();
        break;
    case 'delete':
        await deleteApp();
        break;
    case 'delete-all':
        await deleteAllApps();
        break;
    case 'list':
        await listApps();
        break;
}