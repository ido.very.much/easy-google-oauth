import { appAccount } from "./puppeteer/const.js";
import listAppsPuppeteer, { printForUser } from "./puppeteer/listApps.js";

export default async function listApps(){
    const appInfo = await listAppsPuppeteer(appAccount.email, appAccount.password);
    await printForUser(...appInfo);
}