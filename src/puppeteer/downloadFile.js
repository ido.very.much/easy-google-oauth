import fs from 'fs-extra';
import path from 'path';
import os from 'os';
import { v4 as uuid } from 'uuid';
import { waitSeconds } from './basic.js';

const tempDir = path.join(os.tmpdir(), uuid());

export default async function downloadFile(page, downloadCallback){
    const client = await page.target().createCDPSession();
    await client.send('Page.setDownloadBehavior', {
        behavior: 'allow',
        downloadPath: tempDir,
    });

    await fs.ensureDir(tempDir);
    await downloadCallback();
    await waitSeconds(8);

    const files = await fs.readdir(tempDir);
    const filePath = path.join(tempDir, files[0]);
    const fileContent = await fs.readFile(filePath);

    await fs.remove(tempDir);
    return fileContent;
}