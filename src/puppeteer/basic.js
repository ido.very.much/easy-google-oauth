import puppeteer from 'puppeteer';
import { $ } from 'execa';
import got from 'got';
import path from 'path';
import fs from 'fs-extra';
import os from 'os';
import { v4 as uuid } from 'uuid';
import fkill from 'fkill';
import input from 'input';
import { closeOnEnd } from './const.js';

export function waitSeconds(seconds) {
    return new Promise(r => setTimeout(r, 1000 * seconds));
}

async function findChromeUrl() {
    if (os.type() === 'Linux' && os.arch() === 'arm64')
        return '/usr/bin/chromium-browser';

    let chromePath = path.resolve('./node_modules/puppeteer/.local-chromium/');

    const version = (await fs.readdir(chromePath)).pop();
    const insideVersion = path.join(chromePath, version);

    if (os.type() === "Darwin") {
        return path.join(insideVersion, 'chrome-mac/Chromium.app/Contents/MacOS/Chromium');
    }

    return path.join(insideVersion, 'chrome-win/chrome');
}

function fetchWebSocketDebuggerUrl() {
    return got('http://127.0.0.1:9922/json/version').json();
}

export default async function clickAccess(accessURL, email, password, backToMyPage = true) {
    const temDir = path.join(os.tmpdir(), uuid());

    try {
        await fetchWebSocketDebuggerUrl();
    } catch {
        $({ detached: true })`${await findChromeUrl()} --remote-debugging-port=9922 --no-first-run --no-default-browser-check --user-data-dir=${temDir}`;
        await waitSeconds(4);
    }

    const { webSocketDebuggerUrl } = await fetchWebSocketDebuggerUrl();
    const browser = await puppeteer.connect({
        browserWSEndpoint: webSocketDebuggerUrl,
        ignoreHTTPSErrors: true
    });

    let page = (await browser.pages())[0];

    await page.goto(accessURL, { waitUntil: 'networkidle2' });

    if (page.url().startsWith('https://accounts.google.com/') && !await findClick(page, 'span', 'choose your account or a brand account')) {
        // Wait for email input.
        await page.waitForSelector('#identifierId');
        //login to account
        await page.goto(accessURL);
        await page.type('#identifierId', email);
        await waitSeconds(1);
        await page.keyboard.press('Enter');

        // Wait for password input
        await waitSeconds(2);
        await page.type('input[type="password"]', password);
        await waitSeconds(1);
        await page.keyboard.press('Enter');
        await waitSeconds(3);

        if (page.url() !== accessURL) {
            await input.text('Please confirm your login to continue');
        }

        if (backToMyPage) {
            await page.goto(accessURL, { waitUntil: 'networkidle2' });
        }
    }

    return {
        page,
        async close() {
            if (!closeOnEnd) {
                console.log('Skipping cleanup...');
                browser.disconnect();
                return;
            }

            console.log('Cleaning up...');

            await browser.close();
            await fkill('Chromium', {
                tree: true,
                forceAfterTimeout: 1000 * 3,
                silent: true
            });

            await waitSeconds(2);

            await fs.remove(temDir);
        }
    }
}

export async function clickLast(page, query) {
    return await page.evaluate((query) => {
        try {
            [...document.querySelectorAll(query)].pop().click();
            return true;
        } catch { }
    }, query);
}

export async function findClick(page, query, text) {
    return await page.evaluate((query, text) => {
        try {
            for (const i of document.querySelectorAll(query)) {
                if (i.innerText.toLowerCase().trim() === text) {
                    i.click();
                    return true;
                }
            }
        } catch { }
    }, query, text);
}