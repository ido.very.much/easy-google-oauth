import loginPage, { waitSeconds, clickLast } from './basic.js';


export default async function connectUser(accessURL, email, password) {

    const { page, close } = await loginPage(accessURL, email, password, false);

    await waitSeconds(6);
    if (await clickLast(page, `[data-email="${email}"]`)) {
        page.waitForNetworkIdle();
        await waitSeconds(3);
    }

    await clickLast(page, 'a');

    await waitSeconds(2);
    await clickLast(page, '[type="button"]');

    await close();
}