import loginPage, { waitSeconds, clickLast } from './basic.js';
import input from 'input';
import { findClick } from './basic.js';


export default async function deleteApp(email, password, defaultProjectId) {

    const projectId = defaultProjectId || await input.text('Enter project id: ')
    const { page, close } = await loginPage("https://console.cloud.google.com/iam-admin/settings?project="+projectId, email, password);

    await findClick(page, 'button', 'shut down');
    await waitSeconds(.5);

    await page.type('mat-dialog-container input', projectId);
    await clickLast(page, 'mat-dialog-container button');
    await waitSeconds(2);

    return close;
}