import {PORT, credentialsPath} from './generateUserToken.js';
import { appAccount, scopes, endPoints } from './puppeteer/const.js';
import newApp from './puppeteer/newApp.js';
import fs from 'fs-extra';

export default async function createApp(){
    const credentials = await newApp(appAccount.email, appAccount.password, {scopes, endPoints, port: PORT, appName: appAccount.name || "connect-account-app"});
    await fs.writeFile(credentialsPath, credentials);
    console.log('App Created');
}