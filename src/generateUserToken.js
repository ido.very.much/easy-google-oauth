import { google } from 'googleapis';
import express from 'express';
import fs from 'fs-extra';
import path from 'path';
import browser from './puppeteer/connectUser.js';
const { scopes, connectUserAccount } = await fs.readJSON('./account.json');
import process from 'process';

export const PORT = 8090;

const tokens = path.resolve('./tokens');
const passwordPath = path.join(tokens, 'userToken.txt');
export const credentialsPath = path.join(tokens, 'credentials.json');

async function openAuth() {
    const { client_secret, client_id, redirect_uris } = (await fs.readJSON(credentialsPath)).web;
    return new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
}

function openServerToGetToken(accessURL) {
    return new Promise(async response => {
        const app = express();

        let listen, waitToClose;
        app.all('*', (req, res) => {
            res.end('ok');
            listen.close(() => waitToClose.then(() => response(req.query.code)));
        });

        listen = app.listen(PORT, () => console.log(`Listening on port ${PORT} - waiting for access tokens - http://localhost:${PORT}/`));

        waitToClose = browser(accessURL, connectUserAccount.email, connectUserAccount.password);
    });
}

export default async function getAccessToken() {
    const oAuth2Client = await openAuth();
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: scopes,
    });

    console.log('Authorizing this app automatically');
    const oneTimeToken = await openServerToGetToken(authUrl);

    const { tokens } = await oAuth2Client.getToken(oneTimeToken);

    await fs.writeFile(passwordPath, tokens.refresh_token);
    console.log('User refresh token created');

    process.exit(0);
}